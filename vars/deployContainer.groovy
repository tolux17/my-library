def call(){
    sh "echo deploy to UAT environment"
    deploy adapters: [tomcat9(credentialsId: 'tomcatapp', path: '', url: "$TOMCAT_URL")], contextPath: null, war: 'target/*.war'
}